(function () {
    angular
        .module("flApp")
        .controller("DisplayCtrl", DisplayCtrl);

    DisplayCtrl.$inject = ["$http"];

    function DisplayCtrl($http) {
        var vm = this;

        // Initial calls
        init();

        // Function definition
        function init(){
            $http
                .get("/api/friends")
                .then(function (response) {
                    console.log("init data " + JSON.stringify(response.data));
                    vm.result = response.data;
                })
                .catch(function (error) {
                    console.log(error);
                });
        }
    }

})();
