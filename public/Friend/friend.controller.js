(function(){
    var flApp = angular.module("flApp");

    // FlCtrl.$inject = $http;

    var FlCtrl = function($http){
        var flCtrl = this;

        flCtrl.editStatus = false;
        flCtrl.avatarImageUrl = "images/maleAvatar_50x50.jpg";
        flCtrl.contact ={
            op_id: "1",
            op_name: "Your Name",
            op_email: "youremail@domain.com",
            op_contact: "87945634",
            op_gender: "M",
            op_creator: "myemail@domain.com",
            op_photo: "",
            op_group: ""
        };

        flCtrl.style = {
            "background-color": "white"
        };

        flCtrl.select  = function ($index) {

            flCtrl.selected = $index;
            // flCtrl.displayEntry($index);
        };

        flCtrl.getAvatarUrl = function(contact) {
            if (contact.gender == "Male") {
                flCtrl.avatarImageUrl = "images/maleAvatar_50x50.png";
            } else {
                flCtrl.avatarImageUrl = "images/femaleAvatar_50x50.png";
            }
            return flCtrl.avatarImageUrl;
        };

        flCtrl.selectGender = function(){
            if (flCtrl.contact.op_gender === "M") {
                flCtrl.contact.op_photo = "images/maleAvatar_50x50.png";
            } else {
                flCtrl.contact.op_photo = "images/femaleAvatar_50x50.png";
            }
        };

        flCtrl.addToList = function() {
            console.log("--index.js-- addToList");
            console.log(flCtrl.contact);
            $http
                .post("/api/friends", flCtrl.contact)
                .then(function () {
                    console.log("successfully added");
                    flCtrl.init();
                })
                .catch(function () {
                    console.log("fail");
                })
        };


        flCtrl.resetEntry = function(){
            flCtrl.contact.op_name="";
            flCtrl.contact.op_email="";
            flCtrl.contact.op_contact="";
            flCtrl.contact.op_gender="";
            flCtrl.contact.op_creator = "";
            flCtrl.contact.op_photo = "";
            flCtrl.contact.op_group = ""
        };

        function slectClicked(id) {
            for (var i = 0; i < flCtrl.contacts.length; i++){
                if (flCtrl.contacts[i].id === Number(id)){
                    flCtrl.contact.op_id = flCtrl.contacts[i].id;
                    flCtrl.contact.op_name = flCtrl.contacts[i].name;
                    flCtrl.contact.op_email = flCtrl.contacts[i].email;
                    flCtrl.contact.op_contact = flCtrl.contacts[i].contact;
                    flCtrl.contact.op_gender = flCtrl.contacts[i].gender;
                    flCtrl.contact.op_creator = flCtrl.contacts[i].creator;
                    flCtrl.contact.op_photo = flCtrl.contacts[i].photo;
                    flCtrl.contact.op_group = flCtrl.contacts[i].group;
                    break;
                }
            }
        };

        flCtrl.showDetails = function(id){
            slectClicked(id);                               // retrive the object from array
            flCtrl.editStatus = true;
        };

        flCtrl.displayEntry = function(num){
            flCtrl.resetEntry();
            flCtrl.editStatus = false;
        };


        flCtrl.updateEntry = function(){
            $http
                .put("/api/friends/" + flCtrl.contact.op_id, flCtrl.contact)
                .then(function (resp){
                    console.log("update successfully");
                    console.log(resp);
                    flCtrl.init();
                    flCtrl.editStatus = false;
                    flCtrl.resetEntry();

                })
                .catch(function(err){
                    console.log("update failed");
                    console.log(err);
                });
        };

        flCtrl.deleteClicked = function(id) {
            slectClicked(id);
            console.log("--index.js-- deleteClicked() " + id);
            console.log("Friend " + flCtrl.contact.op_name + " is to be deleted.", flCtrl.contact);
        };

        flCtrl.deleteFriend = function(){
            console.log("--index.js-- deleteFriend()");
            $http
                .delete("/api/friends/" + flCtrl.contact.op_id)
                .then(function () {
                    console.log("Successfully deleted");
                    flCtrl.init();
                })
                .catch(function () {
                    console.log("Delete fail");
                })
        };

        flCtrl.init = function(){
            $http
                .get("/api/friends")
                .then(function (response) {
                    console.log("init data " + JSON.stringify(response.data));
                    flCtrl.contacts = response.data;
                })
                .catch(function (error) {
                    console.log(error);
                });
        };

        flCtrl.init();
    };

    flApp.controller("FlCtrl", ["$http", FlCtrl]);


})();
