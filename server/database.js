var Sequelize = require("sequelize");
var config = require('./config');

var database = new Sequelize(
    config.mysql.database,
    config.mysql.username,

    config.mysql.password, {
        host:config.mysql.host,
        dialect:"mysql",
        pool:{
            max: 4,
            min: 0,
            idle:10000
        },
        force:false
    });

var FriendModel = require('./api/Friend/friend.model')(database);
var UserModel = require('./api/User/user.model')(database);

// FriendModel.hasOne(UserModel, {foreignKey: 'email'}); //To confirm foreignKey****

database.sync()
    .then(function () {
        console.log("DB in sync")
    });

module.exports = {
    Friend: FriendModel,
    User: UserModel
};

