'use strict';

var express = require("express");
var path = require("path");
var FriendController = require("./api/Friend/friend.controller");
var UserController = require("./api/User/user.controller");

const CLIENT_FOLDER = path.join(__dirname + '/../public');


module.exports = {
    init: configureRoutes,
    errorHandler: errorHandler
};

function configureRoutes(app){

    app.post("/api/friends", FriendController.create);
    app.delete("/api/friends/:friendId", FriendController.delete);
    app.get("/api/friends", FriendController.list);
    // app.get("/api/department_number/:dept_no", DepartmentController.get);
    app.put("/api/friends/:friendId", FriendController.update);

    // app.delete("/api/department_manager/:emp_no", ManagerController.delete);


    app.use(express.static(CLIENT_FOLDER));

}

function errorHandler(app) {
    app.use(function (req, res) {
        res.status(401).sendFile(CLIENT_FOLDER + "/app/errors/404.html");
    });

    app.use(function (err, req, res, next) {
        res.status(500).sendFile(path.join(CLIENT_FOLDER + '/app/errors/500.html'));
    });
};

