var Friend = require("../../database").Friend;


// Creating new contacts
exports.create = function(req,res){
    console.log("reached server");
    console.log(req.body);
    Friend
        .create({
            name: req.body.op_name,
            email:req.body.op_email,
            contact: req.body.op_contact,
            gender: req.body.op_gender,
            creator: req.body.op_creator,
            photo: req.body.op_photo,
            group: req.body.op_group
        })
        .then(function(friend){
            console.log("sent");
            res
                .status(200)
                .json(friend)
        })
        .catch(function(err){
            console.log(err);
            res
                .status(500)
                .json({error: true})
        })

};

exports.delete = function(req,res){
    console.log("--reached server--delete");
    console.log(req);
    Friend
        .destroy({
            where: {
                id: req.params.friendId
            }

        })
        .then(function(friend) {
            console.log("sent");
            res
                .status(200)
                .json(friend)
        })
        .catch(function(err){
            console.log("err", err);
            res
                .status(500)
                .json({error: true})
        })

};

exports.update = function(req,res){
    console.log("--reached server--update()");
    console.log(req.params);
    Friend
        .find({
            where: {
                id: req.params.friendId
            }
        })
        .then(function(response){
            console.log('DB Response', response);
            response.updateAttributes({
                name: req.body.op_name,
                email:req.body.op_email,
                contact: req.body.op_contact,
                gender: req.body.op_gender,
                creator: req.body.op_creator,
                photo: req.body.op_photo,
                group: req.body.op_group
            }).then(function (response){
                console.log(response);
                res.status(200);
            }).catch(function (error){
                console.log(error);
                res
                    .status(500)
                    .json({error: true, errorText: "Update Failed"})
            });
        })
        .catch(function(err){
            console.log("err", err);
            res
                .status(500)
                .json({error: true, errorText: "Record not found"})
        });

};

exports.list = function(req,res){
    console.log("reached database >> list 1st 20 contacts");
    console.log(req.body);
    Friend
        .findAll({
            limit: 20
        })
        .then(function (result) {
            if (result) {
                res.json(result);
            } else {
                res.status(400).send(JSON.stringify("Record Not Found"));
            }
        });
};

