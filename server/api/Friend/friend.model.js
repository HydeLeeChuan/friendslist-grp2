var Sequelize = require("sequelize");

module.exports = function (database) {

    var Friend = database.define("friends", {

        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        name: {
            type: Sequelize.STRING
        },
        email: {
            type: Sequelize.STRING
        },
        contact: {
            type: Sequelize.STRING
        },
        gender: {
            type: Sequelize.ENUM('M','F')
        },
        creator: {
            type: Sequelize.STRING
        },
        photo: {
            type: Sequelize.STRING
        },
        group: {
            type: Sequelize.STRING
        }
    },{
        timestamps:true
    });

    return Friend;

};