var Sequelize = require("sequelize");

module.exports = function (database) {


    var User = database.define("users", {

        name: {
            type: Sequelize.STRING
        },

        email: {
            type: Sequelize.STRING,
            primaryKey: true // set email as primaryKey
        },

        contact: {
            type: Sequelize.STRING
        },
        gender: {
            type: Sequelize.ENUM('M','F')
        },

        password:{
            type: Sequelize.STRING
        },
        photo:{
            type: Sequelize.STRING
        }
    },{
        timestamps:true
    });

    return User;

};

